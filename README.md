# syslinux

Collection of bootloaders: SYSLINUX (DOS FAT, NTFS), EXTLINUX (ext2-3-4, btrfs, xfs), PXELINUX (network), ISOLINUX (CD-ROMs). https://en.m.wikipedia.org/wiki/SYSLINUX

# Official documentation
* https://syslinux.org

# Unofficial documentation
* [*Anatomy of a Bootable ISO*
  ](http://www.better-bsp.com/blog/2017/03/03/anatomy-of-a-bootable-iso/)
  2017-03 Tony Arkles